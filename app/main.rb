


class Game
  attr_accessor :ouputs
  def initialize
    @player = Player.new
  end

  def update args
    @player.update controller_info args
  end

  def render args
    args.outputs.solids << [0, 0, 1280,720, 15,0,20,255]
    args.outputs.primitives << @player.sprite.solid
  end
  def collision_detection
    player_box = [@player[:x], @player[:y], @player[:w], @player[:h]]
    # Border collision
    @player.x = 0 if @player.x < 0
    @player.x = 1280 - @player.w if @player.x + @player.w > 1280
    @player.y = 0 if @player.y < 0
    @palyer.y = 720 - @player.h if @player.y + @player.h > 720
  end
end
class Entity
  def initalize(x, y, style)

  end
end

class Player
  attr_accessor :sprite, :x, :y, :w, :h
  def initialize
    @w = 50
    @h = 50
    @x = 640
    @y = 360
    @sprite = {}
  end
  def update control
    dx = control[:dx] / 1000
    dy = control[:dy] / 1000
    if dx != 0 || dy != 0
      @x += dx
      @y += dy
    else
      @x = control[:mouse_x] 
      @y = control[:mouse_y]
    end

    @sprite = {
      :x => @x,
      :y => @y,
      :w => @w,
      :h => @h,
      :r => 125,
      :g => 25,
      :b => 25,
      :a => 255} 
  end

  def on_click

  end
end
def controller_info args
  control = {
    :dx => args.inputs.controller_one.left_analog_x_raw,
    :dy => args.inputs.controller_one.left_analog_y_raw,
    :click => args.inputs.mouse.click,
    :mouse_x => args.inputs.mouse.x,
    :mouse_y => args.inputs.mouse.y}
  return control
end



$game = Game.new

def tick args
  args.outputs.static_background_color ||= [15,0,20]
  $game.update args
  $game.render args


end
